package datamining;

import java.awt.Color;

//Classe che rappresenta un singolo pixel di un'immagine
//usata per il clustering di immagini
//il suo colore e la sua posizione
public class RGBpixel extends Color implements Confrontabile{
    private int x;
    private int y;
    
    public RGBpixel(int x,int y,int r, int g, int b) {
        super(r, g, b);
        this.x=x;
        this.y=y;
    }

    @Override
    public Double[] toVector() {
        Double[] d=new Double[3];
        d[0]=(double)this.getRed();
        d[1]=(double)this.getGreen();
        d[2]=(double)this.getBlue();
        return d;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }    
}
