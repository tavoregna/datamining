package datamining;

import java.io.Serializable;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

//classe che rappresenta un singolo elemento (punto) del dataset italy_earthquakes_from_2016-08-24_to_2016-11-30.csv
//ovvero l'indicazione della longitudine e della latitudine dell'epicentro di un terremoto
public class Italy_Earthquakes implements Serializable,Confrontabile{
    			
    private double latitude;
    private double longitude;
    private int id;
    
    public static Encoder<Italy_Earthquakes> getEncoder() {
    return Encoders.bean(Italy_Earthquakes.class);
  }

    public Italy_Earthquakes(int id, double latitude, double longitude){
        this.id=id;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Italy_Earthquakes() {
    }

     public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Italy_Earthquakes other = (Italy_Earthquakes) obj;
        if (Double.doubleToLongBits(this.longitude) != Double.doubleToLongBits(other.longitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        return true;
    }
    
    @Override
    public Double[] toVector()
    {
       Double[] v=new Double[2];
       v[0]=latitude;
       v[1]=longitude;
       return v;
    }

    @Override
     public String toString() {
        return "Earthquakes: " +id+ ", latitude: " + latitude + ", longitude: " + longitude;
    }
}
