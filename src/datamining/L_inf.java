package datamining;

//classe che implementa la funzione di distanza L-infinity norm
public class L_inf extends Distance{

    @Override
    public double distanceBetweenPoints(Object t1, Object t2) {
        if(!t1.getClass().equals(t2.getClass()) || !(t1 instanceof Confrontabile))
        {
            System.out.println("oggetti non comparabili");
            System.exit(1);
        }
        
        Double[] x=((Confrontabile)t1).toVector();
        Double[] y=((Confrontabile)t2).toVector();
        int n=x.length;
        double max=0;
        for(int i=0;i<n;i++)
        {
            double d=Math.abs(x[i]-y[i]);
            if(d>max)
                max=d;
        }
        return max;
    }   
}