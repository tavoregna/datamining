package datamining;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.imageio.ImageIO;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;

//classe che contiene metodi statici che effettuano il parsing dei file contenenti
//i dataset e producono il relativo JavaRDD
public class DataInput {
    
    //per i dataset .tsp
    public static JavaRDD<TSPVertex> readTSPVertex(JavaSparkContext sc, String path) {
        return sc
            .textFile(path)
            .flatMap((t) -> {
                List<TSPVertex> l=new ArrayList<>();
                String[] parts = t.split(":");
                if (parts.length==2){
                    if(parts[0].trim().equals("EDGE_WEIGHT_TYPE") && !parts[1].trim().equals("EUC_2D"))
                    {
                        System.err.println("unknown EDGE_WEIGHT_TYPE");
                        System.exit(1);
                    }
                    return l.iterator();
                }
                parts = t.split(" ");
                if (parts.length>=3){
                    TSPVertex ver=new TSPVertex();
                    int i=0;

                    for(int k=0;k<parts.length;k++)
                    {
                        try{
                            if(i==0)
                                ver.setId(Integer.parseInt(parts[k].trim()));
                            else if(i==1)
                                ver.setX(Double.parseDouble(parts[k].trim()));
                            else if(i==2)
                                {ver.setY(Double.parseDouble(parts[k].trim()));break;}
                            i++;
                        }
                        catch(Exception ex){}
                    }                      

                    if(i==2)
                        l.add(ver);
                    return l.iterator();
                }
            return l.iterator();
            })
            .repartition(sc.defaultParallelism())
            .cache();
    }
    
    //per il dataset parkinsons_updrs.data
    public static JavaRDD<ParkinsonUPDRS> readParkinsonUPDRS(JavaSparkContext sc, String path) {
        return sc
            .textFile(path)
            .map((String t1) -> {
                String[] parts = t1.split(",");
                ParkinsonUPDRS p=new ParkinsonUPDRS();
                p.setSubject(Integer.parseInt(parts[0].trim()));
                p.setAge(Integer.parseInt(parts[1].trim()));
                p.setSex(Integer.parseInt(parts[2].trim()));
                p.setTest_time(Double.parseDouble(parts[3].trim()));
                p.setMotors_UPDRS(Double.parseDouble(parts[4].trim()));
                p.setTotal_UPDRS(Double.parseDouble(parts[5].trim()));
                p.setJitter(Double.parseDouble(parts[6].trim()));
                p.setJitter_Abs(Double.parseDouble(parts[7].trim()));
                p.setJitter_RAP(Double.parseDouble(parts[8].trim()));
                p.setJitter_PPQ5(Double.parseDouble(parts[9].trim()));
                p.setJitter_DDP(Double.parseDouble(parts[10].trim()));
                p.setShimmer(Double.parseDouble(parts[11].trim()));
                p.setShimmer_dB(Double.parseDouble(parts[12].trim()));
                p.setShimmer_APQ3(Double.parseDouble(parts[13].trim()));
                p.setShimmer_APQ5(Double.parseDouble(parts[14].trim()));
                p.setShimmer_APQ11(Double.parseDouble(parts[15].trim()));
                p.setShimmer_DDA(Double.parseDouble(parts[16].trim()));
                p.setNHR(Double.parseDouble(parts[17].trim()));
                p.setHNR(Double.parseDouble(parts[18].trim()));
                p.setDFA(Double.parseDouble(parts[19].trim()));
                p.setRPDE(Double.parseDouble(parts[20].trim()));
                p.setPPE(Double.parseDouble(parts[21].trim()));
            return p;
        })
        .repartition(sc.defaultParallelism())
        .cache();
    }
    
    //per il clustering su immagini
    public static JavaRDD<RGBpixel> readRGBpixel(JavaSparkContext sc, String path) {
        try {
            List<RGBpixel> li=new ArrayList<>();   
            
            File file = new File(path);
            BufferedImage img;
            img = ImageIO.read(file);
            for(int i=0;i<img.getWidth();i++)
                for(int j=0;j<img.getHeight();j++)
                {
                    int p=img.getRGB(i, j);
                    int red = (p>> 16) & 0x000000FF;
                    int green = (p>>8 ) & 0x000000FF;
                    int blue = (p) & 0x000000FF;
                    li.add(new RGBpixel(i,j,red, green, blue));
                }
            
            return sc.parallelize(li)
                .repartition(sc.defaultParallelism())
                .cache();
                        
        }
        catch (IOException ex)
        {
            System.err.println("Errore lettura immagine\n"+ex.getMessage());
            System.exit(1);
            return null;
        }
    }
    
    //per il dataset italy_earthquakes_from_2016-08-24_to_2016-11-30.csv
    public static JavaRDD<Italy_Earthquakes> readItalyEarthquakes(JavaSparkContext sc, String path) {
        AtomicInteger i=new AtomicInteger(0);
        return sc
            .textFile(path)
            .map((String t1) -> {
                String[] parts = t1.split(",");
                Italy_Earthquakes p=new Italy_Earthquakes();
                p.setId(i.get());
                i.addAndGet(1);
                p.setLatitude(Double.parseDouble(parts[1].trim()));
                p.setLongitude(Double.parseDouble(parts[2].trim()));
            return p;
        })
        .repartition(sc.defaultParallelism())
        .cache();
    }
    
    //per il dataset hail-2015.csv
    public static JavaRDD<Weather> readWeather(JavaSparkContext sc, String path) {
        AtomicInteger i=new AtomicInteger(0);
        return sc
            .textFile(path)
            .map((String t1) -> {
                String[] parts = t1.split(",");
                Weather p=new Weather();
                p.setId(i.get());
                i.addAndGet(1);
                p.setLongitude(Double.parseDouble(parts[1].trim()));
                p.setLatitude(Double.parseDouble(parts[2].trim()));
            return p;
        })
        .repartition(sc.defaultParallelism())
        .cache();
    }
}
