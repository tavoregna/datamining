package datamining;

import java.util.List;

//Rappresenta l'outpur dell'algoritmo GREEDY e GREEDY1 (non GREEDY_MR)
//l'output è costituito da:
// - insieme (lista) di k centri
// - lista di coppie punto-centro più vicino a quel punto
//la lista di coppie viene utilizzata per rendere più efficiente gli algoritmi GREEDY, GREEDY1 e GREEDY_for_greedy_mr
//vedi la classe PointCenter per maggiori dettagli
class GREEDY_OUTPUT<T> {
    private final List<T> center;
    private final List<PointCenter<T>> pointCenter;

    public GREEDY_OUTPUT(List<T> center, List<PointCenter<T>> pointCenter) {
        this.center = center;
        this.pointCenter = pointCenter;
    }

    public List<T> getCenter() {
        return center;
    }

    public List<PointCenter<T>> getPointCenter() {
        return pointCenter;
    }   
}
