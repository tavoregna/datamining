package datamining;

import java.util.List;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class DataMining {

 
    public static void main(String[] args) {
        
        
        System.setProperty("hadoop.home.dir", ".\\null\\bin\\winutils.exe");
        SparkConf sparkConf = new SparkConf(true).setAppName("Progetto Data Mining");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        
        int k=0;
        int z=0;
        double G=0;
        Distance d=new EuclideanDistance();
        
        /*
            ISTRUZIONI PER L'USO
            1. "scommentare"/"commentare" i blocchi relativi agli algoritmi che si vogliono/non si vogliono eseguire
            2. una volta scelto il dataset, bisogna mettere nella variabile datapath il giusto percorso del dataset
            3. impostare tra parentesi angolari  (<  >) il giusto tipo di dato, relativo al dataset scelto
            4. settare opportunamente (da codice o da riga di comando) i parametri k,z,m e soprattutto G
        
            NOTA: è possibile rappresentare graficamente i risultati del clustering solo per i dataset .tsp
        */
        
        int num_par=args.length;
        if(num_par==0)
        {
            System.out.println("parametri non validi");
            System.exit(1);
        }
        if(num_par>=1)
        {
            try
            {
                k=Integer.parseInt(args[0]);
            }
            catch(Exception e)
            {
                System.out.println("parametro k non valido");
                System.exit(1);
            }
        }       
        if(num_par>=2)
        {
            try
            {
                z=Integer.parseInt(args[1]);
            }
            catch(Exception e)
            {
                System.out.println("parametro z non valido");
                System.exit(1);
            }
        }     
        if(num_par>=3)
        {
            try
            {
                G=Double.parseDouble(args[2]);
            }
            catch(Exception e)
            {
                System.out.println("parametro G non valido");
                System.exit(1);
            }
        }

        
        
        //per testare OUTLIER_MR (distribuito)
        
        String dataPath1=".\\data\\tsp\\p654.tsp";
        JavaRDD<TSPVertex> u1=DataInput.readTSPVertex(sc, dataPath1);
        long t11=System.currentTimeMillis();
        OUTLIER_OUTPUT<TSPVertex> x1=Algoritmi.OUTLIERS_MR(sc, u1, k, z, G, d);
        long t21=System.currentTimeMillis();
        PARTITION_OUTPUT<TSPVertex> po1=Algoritmi.PARTITION_MR(sc, u1, x1.getCenter(), x1.getOutlier(), d);
        long t31=System.currentTimeMillis();
        double sil1=Algoritmi.Silhouette_coefficient(po1.getClusters(), d);
        String info1="Algoritmo: OUTLIERS_MR      Dataset: "+dataPath1
                +"\nn="+u1.count()+"       k="+k+"       m="+sc.defaultParallelism()+"       z="+z+"       G="+G
                +"\nTotal duration: "+((t31-t11)/1000.0)+" sec"
                +"      Phase 1: "+((t21-t11)/1000.0)+" sec      Phase 2: "+((t31-t21)/1000.0)+" sec"
                +"\nobj val: "+po1.getObj_val()
                +"\noutlier identified: "+x1.getOutlier().size()
                +"\nSilhouette coefficient: "+sil1;
        System.out.println(info1);
        Grafici.grafico(po1.getClusters(), x1.getCenter(),x1.getOutlier(),info1);
        
  
        
        
        //per testare GREEDY_MR (distribuito)
        
        String dataPath2=".\\data\\tsp\\p654.tsp";
        JavaRDD<TSPVertex> u2=DataInput.readTSPVertex(sc, dataPath2);
        long t12=System.currentTimeMillis();
        List<TSPVertex> x2=Algoritmi.GREEDY_MR(sc, u2, k, d);
        long t22=System.currentTimeMillis();
        PARTITION_OUTPUT<TSPVertex> po2=Algoritmi.PARTITION_MR(sc, u2, x2, d);
        long t32=System.currentTimeMillis();
        double sil2=Algoritmi.Silhouette_coefficient(po2.getClusters(), d);
        String info2="Algoritmo: GREEDY_MR      Dataset: "+dataPath2
                +"\nn="+u2.count()+"       k="+k+"       m="+sc.defaultParallelism()
                +"\nTotal duration: "+((t32-t12)/1000.0)+" sec"
                +"      Phase 1: "+((t22-t12)/1000.0)+" sec      Phase 2: "+((t32-t22)/1000.0)+" sec"
                +"\nobj val: "+po2.getObj_val()
                +"\nSilhouette coefficient: "+sil2;
        System.out.println(info2);
        Grafici.grafico(po2.getClusters(), x2,info2);
        
   
        
        
        //per testare OUTLIER (sequenziale)
        /*
        String dataPath3=".\\data\\tsp\\p654.tsp";
        JavaRDD<TSPVertex> u3=DataInput.readTSPVertex(sc, dataPath3);
        List<TSPVertex> l3=u3.collect();
        long t13=System.currentTimeMillis();
        OUTLIER_OUTPUT<TSPVertex> x3=Algoritmi.OUTLIERS(l3, k, G, d);
        long t23=System.currentTimeMillis();
        PARTITION_OUTPUT<TSPVertex> po3=Algoritmi.PARTITION(l3, x3.getCenter(), x3.getOutlier(), d);
        long t33=System.currentTimeMillis();
        double sil3=Algoritmi.Silhouette_coefficient(po3.getClusters(), d);
        String info3="Algoritmo: OUTLIERS      Dataset: "+dataPath3
                +"\nn="+u3.count()+"       k="+k+"       m="+sc.defaultParallelism()+"       z="+z+"       G="+G
                +"\nTotal duration: "+((t33-t13)/1000.0)+" sec"
                +"      Phase 1: "+((t23-t13)/1000.0)+" sec      Phase 2: "+((t33-t23)/1000.0)+" sec"
                +"\nobj val: "+po3.getObj_val()
                +"\noutlier identified: "+x3.getOutlier().size()
                +"\nSilhouette coefficient: "+sil3;
        System.out.println(info3);
        Grafici.grafico(po3.getClusters(), x3.getCenter(),x3.getOutlier(),info3);
        */
        
        
        
        //per testare GREEDY (sequenziale)
        /*
        String dataPath4=".\\data\\tsp\\p654.tsp";
        JavaRDD<TSPVertex> u4=DataInput.readTSPVertex(sc, dataPath4);
        List<TSPVertex> l4=u4.collect();
        long t14=System.currentTimeMillis();
        GREEDY_OUTPUT<TSPVertex> x4=Algoritmi.GREEDY(l4, k, d);
        long t24=System.currentTimeMillis();
        PARTITION_OUTPUT<TSPVertex> po4=Algoritmi.PARTITION(x4.getPointCenter(),k);
        long t34=System.currentTimeMillis();
        double sil4=Algoritmi.Silhouette_coefficient(po4.getClusters(), d);
        String info4="Algoritmo: GREEDY      Dataset: "+dataPath4
                +"\nn="+u4.count()+"       k="+k+"       m="+sc.defaultParallelism()
                +"\nTotal duration: "+((t34-t14)/1000.0)+" sec"
                +"      Phase 1: "+((t24-t14)/1000.0)+" sec      Phase 2: "+((t34-t24)/1000.0)+" sec"
                +"\nobj val: "+po4.getObj_val()
                +"\nSilhouette coefficient: "+sil4;
        System.out.println(info4);
        Grafici.grafico(po4.getClusters(), x4.getCenter(),info4);
        */
        
        
        
        //k centri scelti casualmente
        /*
        String dataPath5=".\\data\\tsp\\p654.tsp";
        JavaRDD<TSPVertex> u5=DataInput.readTSPVertex(sc, dataPath5);
        long t15=System.currentTimeMillis();
        List<TSPVertex> x5=Algoritmi.randomCenter(u5, k);
        long t25=System.currentTimeMillis();
        PARTITION_OUTPUT<TSPVertex> po5=Algoritmi.PARTITION_MR(sc, u5, x5, d);
        long t35=System.currentTimeMillis();
        double sil5=Algoritmi.Silhouette_coefficient(po5.getClusters(), d);
        String info5="Algoritmo: k centri casuali      Dataset: "+dataPath5
                +"\nn="+u5.count()+"       k="+k+"       m="+sc.defaultParallelism()+"       z="+z+"       G="+G
                +"\nTotal duration: "+((t35-t15)/1000.0)+" sec"
                +"      Phase 1: "+((t25-t15)/1000.0)+" sec      Phase 2: "+((t35-t25)/1000.0)+" sec"
                +"\nobj val: "+po5.getObj_val()
                +"\nSilhouette coefficient: "+sil5;
        System.out.println(info5);
        Grafici.grafico(po5.getClusters(), x5,info5);
        */
    }
}