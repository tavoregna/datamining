package datamining;
import java.io.Serializable;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

//Classe che rappresenta un singolo elemento dei dataset situati 
//nei file con estensione .tsp
//(è un punto nel piano cartesiano)
public class TSPVertex implements Serializable,Confrontabile{
    private double x;
    private double y;
    private int id;

    public TSPVertex(double x, double y, int id) {
        this.x = x;
        this.y = y;
        this.id = id;
    }
    
    public TSPVertex() {
    }
    
    public static Encoder<TSPVertex> getEncoder() {
    return Encoders.bean(TSPVertex.class);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public Double[] toVector()
    {
       Double[] v=new Double[2];
       v[0]=x;
       v[1]=y;
       return v;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TSPVertex other = (TSPVertex) obj;
        if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "vertex: " +id+ ", x: " + x + ", y: " + y;
    }
}
