package datamining;

import java.util.List;

//classe che rappresenta l'output degli algoritmi PARTITION e PARTITION_MR
//eseguiti per "smistare" i punti nei vari cluster una volta trovati i k centri
//l'output è costituito da:
// - il valore della funzione obiettivo
// - k insiemi di punti ovvero i k cluster

public class PARTITION_OUTPUT<T>{
    private final double obj_val;
    private final List<List<T>> clusters;

    public PARTITION_OUTPUT(double obj_val, List<List<T>> clusters) {
        this.obj_val = obj_val;
        this.clusters = clusters;
    }

    public double getObj_val() {
        return obj_val;
    }

    public List<List<T>> getClusters() {
        return clusters;
    }
}
