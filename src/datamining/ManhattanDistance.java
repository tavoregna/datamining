package datamining;

//classe che implementa la funzione di distanza Manhattan
public class ManhattanDistance extends Distance{

    @Override
    public double distanceBetweenPoints(Object t1, Object t2) {
        if(!t1.getClass().equals(t2.getClass()) || !(t1 instanceof Confrontabile))
        {
            System.out.println("oggetti non comparabili");
            System.exit(1);
        }
        
        Double[] x=((Confrontabile)t1).toVector();
        Double[] y=((Confrontabile)t2).toVector();
        int n=x.length;
        double val=0;
        for(int i=0;i<n;i++)
        {
            val+=Math.abs(x[i]-y[i]);
        }
        return val;
    }   
}