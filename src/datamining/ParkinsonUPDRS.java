package datamining;

import java.io.Serializable;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;

//classe che rappresenta un singolo elemento (punto)
//del dataset parkinsons_updrs.data
//vedi descrizione del dataset per maggiori dettagli
public class ParkinsonUPDRS implements Serializable,Confrontabile{
    			
    private int subject;
    private int age;
    private int sex;
    private double test_time;
    private double motor_UPDRS;
    private double total_UPDRS;
    private double Jitter;
    private double Jitter_Abs;
    private double Jitter_RAP;
    private double Jitter_PPQ5;
    private double Jitter_DDP;
    private double Shimmer;
    private double Shimmer_dB;
    private double Shimmer_APQ3;
    private double Shimmer_APQ5;
    private double Shimmer_APQ11;
    private double Shimmer_DDA;
    private double NHR;
    private double HNR;
    private double RPDE;
    private double DFA;
    private double PPE;
    
    public static Encoder<ParkinsonUPDRS> getEncoder() {
    return Encoders.bean(ParkinsonUPDRS.class);
  }

    public ParkinsonUPDRS(int subject, int age, int sex, double test_time, double motor_UPDRS, double total_UPDRS, double Jitter,double Jitter_Abs,double Jitter_RAP,double Jitter_PPQ5,double Jitter_DDP,double Shimmer,double Shimmer_dB,double Shimmer_APQ3,double Shimmer_APQ5,double Shimmer_APQ11,double Shimmer_DDA,double NHR,double HNR, double RPDE,double DFA,double PPE) {   
        this.subject=subject;
        this.age=age;
        this.sex=sex;
        this.test_time=test_time;
        this.motor_UPDRS=motor_UPDRS;
        this.total_UPDRS=total_UPDRS;
        this.Jitter = Jitter;
        this.Jitter_Abs = Jitter_Abs;
        this.Jitter_RAP = Jitter_RAP;
        this.Jitter_PPQ5 = Jitter_PPQ5;
        this.Jitter_DDP = Jitter_DDP;
        this.Shimmer = Shimmer;
        this.Shimmer_dB = Shimmer_dB;
        this.Shimmer_APQ3 = Shimmer_APQ3;
        this.Shimmer_APQ5 = Shimmer_APQ5;
        this.Shimmer_APQ11 = Shimmer_APQ11;
        this.Shimmer_DDA = Shimmer_DDA;
        this.NHR = NHR;
        this.HNR = HNR;
        this.DFA=DFA;
        this.RPDE = RPDE;
        this.PPE = PPE;
    }

    public ParkinsonUPDRS() {
    }

     public double getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public double getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    public double getSex() {
        return sex;
    }
    
    public void setSex(int sex) {
        this.sex= sex;
    }
    
    public double getTest_time() {
        return test_time;
    }

    public void setTest_time(double test_time) {
        this.test_time = test_time;
    }

    public double getMotors_UPDRS() {
        return motor_UPDRS;
    }

    public void setMotors_UPDRS(double motors_UPDRS) {
        this.motor_UPDRS = motors_UPDRS;
    }
    
    public double getTotal_UPDRS() {
        return total_UPDRS;
    }

    public void setTotal_UPDRS(double total_UPDRS) {
        this.total_UPDRS = total_UPDRS;
    }
    
    public double getJitter() {
        return Jitter;
    }

    public void setJitter(double Jitter) {
        this.Jitter = Jitter;
    }

    public double getJitter_Abs() {
        return Jitter_Abs;
    }

    public void setJitter_Abs(double Jitter_Abs) {
        this.Jitter_Abs = Jitter_Abs;
    }
    
    public void setJitter_RAP(double Jitter_RAP) {
        this.Jitter_RAP = Jitter_RAP;
    }

    public double getJitter_PPQ5() {
        return Jitter_PPQ5;
    }

    public void setJitter_PPQ5(double Jitter_PPQ5) {
        this.Jitter_PPQ5 = Jitter_PPQ5;
    }

    public double getJitter_DDP() {
        return Jitter_DDP;
    }

    public void setJitter_DDP(double Jitter_DDP) {
        this.Jitter_DDP = Jitter_DDP;
    }

    public double getShimmer() {
        return Shimmer;
    }

    public void setShimmer(double Shimmer) {
        this.Shimmer = Shimmer;
    }

    public double geShimmer_dB() {
        return Shimmer_dB;
    }

    public void setShimmer_dB(double Shimmer_dB) {
        this.Shimmer_dB = Shimmer_dB;
    }

    public double getShimmer_APQ3() {
        return Shimmer_APQ3;
    }

    public void setShimmer_APQ3(double Shimmer_APQ3) {
        this.Shimmer_APQ3 = Shimmer_APQ3;
    }

    public double getJShimmer_APQ5() {
        return Shimmer_APQ5;
    }

    public void setShimmer_APQ5(double Shimmer_APQ5) {
        this.Shimmer_APQ5 = Shimmer_APQ5;
    }

    public double getShimmer_APQ11() {
        return Shimmer_APQ11;
    }

    public void setShimmer_APQ11(double Shimmer_APQ11) {
        this.Shimmer_APQ11 = Shimmer_APQ11;
    }

    public double getShimmer_DDA() {
        return Shimmer_DDA;
    }

    public void setShimmer_DDA(double Shimmer_DDA) {
        this.Shimmer_DDA = Shimmer_DDA;
    }


    public double getNHR() {
        return NHR;
    }

    public void setNHR(double NHR) {
        this.NHR = NHR;
    }

    public double getHNR() {
        return HNR;
    }

    public void setHNR(double HNR) {
        this.HNR = HNR;
    }

    public double getRPDE() {
        return RPDE;
    }

    public void setRPDE(double RPDE) {
        this.RPDE = RPDE;
    }

    public double getDFA() {
        return DFA;
    }

    public void setDFA(double DFA) {
        this.DFA = DFA;
    }

    public double getPPE() {
        return PPE;
    }

    public void setPPE(double PPE) {
        this.PPE = PPE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ParkinsonUPDRS other = (ParkinsonUPDRS) obj;
        if (Double.doubleToLongBits(this.Jitter) != Double.doubleToLongBits(other.Jitter)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Jitter_PPQ5) != Double.doubleToLongBits(other.Jitter_PPQ5)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_APQ11) != Double.doubleToLongBits(other.Shimmer_APQ11)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Jitter_Abs) != Double.doubleToLongBits(other.Jitter_Abs)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Jitter_RAP) != Double.doubleToLongBits(other.Jitter_RAP)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_APQ3) != Double.doubleToLongBits(other.Shimmer_APQ3)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Jitter_DDP) != Double.doubleToLongBits(other.Jitter_DDP)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer) != Double.doubleToLongBits(other.Shimmer)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_dB) != Double.doubleToLongBits(other.Shimmer_dB)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_APQ3) != Double.doubleToLongBits(other.Shimmer_APQ3)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_APQ5) != Double.doubleToLongBits(other.Shimmer_APQ5)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Shimmer_DDA) != Double.doubleToLongBits(other.Shimmer_DDA)) {
            return false;
        }
        if (Double.doubleToLongBits(this.NHR) != Double.doubleToLongBits(other.NHR)) {
            return false;
        }
        if (Double.doubleToLongBits(this.RPDE) != Double.doubleToLongBits(other.RPDE)) {
            return false;
        }
        if (Double.doubleToLongBits(this.PPE) != Double.doubleToLongBits(other.PPE)) {
            return false;
        }
        return true;
    }
    
    @Override
    public Double[] toVector()
    {
        Double[] ar=new Double[15];
        ar[0]=Jitter;
        ar[1]=Jitter_Abs;
        ar[2]=Jitter_RAP;
        ar[3]=Jitter_PPQ5;
        ar[4]=Jitter_DDP;
        ar[5]=Shimmer;
        ar[6]=Shimmer_dB;
        ar[7]=Shimmer_APQ3;
        ar[8]=Shimmer_APQ5;
        ar[9]=Shimmer_APQ11;
        ar[10]=Shimmer_DDA;
        ar[11]=NHR;
        ar[12]=RPDE;
        ar[13]=DFA;
        ar[14]=PPE;
        return ar;
    }

    @Override
    public String toString() {
        return "ParkinsonUPDRS{" + "subject=" + subject + ", age=" + age + ", sex=" + sex + ", test_time=" + test_time + ", motor_UPDRS=" + motor_UPDRS + ", total_UPDRS=" + total_UPDRS + ", Jitter=" + Jitter + ", Jitter_Abs=" + Jitter_Abs +", Jitter_RAP=" + Jitter_RAP + ", Jitter_PPQ5=" + Jitter_PPQ5 + ", Jitter_DDP=" + Jitter_DDP +", Shimmer=" + Shimmer +", Shimmer_dB=" + Shimmer_dB + ", Shimmer_APQ3=" + Shimmer_APQ3 + ", Shimmer_APQ5=" + Shimmer_APQ5 + ", Shimmer_APQ11=" + Shimmer_APQ11 + ", Shimmer_DDA=" + Shimmer_DDA + ", NHR=" + NHR + ", RPDE=" +", HNR=" + HNR + ", RPDE=" + RPDE + ", DFA=" + DFA + ", spread1=" + ", PPE=" + PPE + '}';
    }
}
