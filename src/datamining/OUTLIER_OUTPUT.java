package datamining;

import java.io.Serializable;
import java.util.List;

//classe che rappresenta l'output degli algoritmi OUTLIER E OUTLIER_MR
//l'output è costituito da:
// - un insieme di k centri
// - un insieme di outlier
public class OUTLIER_OUTPUT<T> implements Serializable{
    private final List<T> center;
    private final List<T> outlier;

    public OUTLIER_OUTPUT(List<T> center, List<T> outlier) {
        this.center = center;
        this.outlier = outlier;
    }

    public List<T> getCenter() {
        return center;
    }

    public List<T> getOutlier() {
        return outlier;
    }
}
