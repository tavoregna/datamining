package datamining;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

//classe che rappresenta una generica funzione di distanza su un certo spazio metrico
//contiene i seguenti metodi:
// - metodo per il calcolo della distanza tra 2 punti (da implementare)
// - metodo per il calcolo della distanza di un punto da un insieme
//   (che  è la minima distanza tra quel punto e ogni punto dell'insieme)
public abstract class Distance<T> implements Serializable{
    
    public abstract double distanceBetweenPoints(T t1,T t2);
    
    public double distancePointAndList(T p1, List<T> x)
    {
        if(!(p1 instanceof Confrontabile))
        {
            System.out.println("errore: Distanza da insieme vuoto");
            System.exit(1);
        }
        
        if(x.isEmpty())
        {
            System.out.println("errore: Distanza da insieme vuoto");
            System.exit(1);
        }
        
        Iterator<T> i=x.iterator();
        double min=distanceBetweenPoints(p1, i.next());
        while(i.hasNext())
        {
            double dist=distanceBetweenPoints(p1, i.next());
            if(dist<min)
            {
                min=dist;
            }
        }
        return min;
    }
}
