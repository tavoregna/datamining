package datamining;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

//classe utilizzata per rappresentare graficamente i risultati del clustering
//fatti sui dataset .tsp che hanno come feature le coordinate x e y
//NOTA: utilizza la libreria JFreeChart
public class Grafici extends ApplicationFrame {
    
    //costruttutore per la visualizzazione a schermo del grafico
    // e impostazione di alcune sue proprietà
    public Grafici(List<List<TSPVertex>> l,List<TSPVertex> cen,XYDataset ds,String tit) {
        super("");
        JFreeChart xylineChart = ChartFactory.createXYLineChart(
            "Clusters",
            "X" ,
            "Y" ,
            ds ,
            PlotOrientation.VERTICAL ,
            true , true , false);
            xylineChart.addSubtitle(new TextTitle(tit,new Font("TimesRoman", Font.BOLD, 11)));
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        double min=Math.min(width, height);
        ChartPanel chartPanel = new ChartPanel(xylineChart);
        chartPanel.setPreferredSize( new java.awt.Dimension((int)(min*9/10),(int)(min*9/10)));
        final XYPlot plot = xylineChart.getXYPlot( );
      
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
        renderer.setBaseLinesVisible(false);    //non mostra le linee di congiunzione tra i punti
        
        renderer.setSeriesPaint( 0 , Color.BLACK );                                           //aspetto per i centri
        renderer.setSeriesPaint( ds.getSeriesCount()-1 , new Color(204, 255, 0));             //aspetto per gli outlier
        
        plot.setRenderer(renderer); 
        setContentPane(chartPanel); 
    }
   
    //creazione dei dataset da mostrare graficamente 
    //ogni serie rappresenta un cluster (e sono k9
    //ci sono 2 serie aggiuntive:
    // - una per rappresentare i centri dei cluster
    // - una per rappresentare gli outlier
    private static XYDataset createDataset(List<List<TSPVertex>> l, List<TSPVertex> center, List<TSPVertex> out) {
       
        XYSeriesCollection dataset = new XYSeriesCollection( ); 
        XYSeries centri = new XYSeries("Centri"); 
        for(int i=0;i<center.size();i++)
        {
           centri.add(center.get(i).getX(),center.get(i).getY());
        }
        dataset.addSeries(centri);
        for(int i=0;i<l.size();i++)
        {
            XYSeries series = new XYSeries("Cluster "+(i+1)); 
            int size=l.get(i).size();
            for(int j=0;j<size;j++)
            {
               TSPVertex v=l.get(i).get(j);
               series.add(v.getX(),v.getY());
            }
            dataset.addSeries(series);
        }
        if(out==null)
            return dataset;
       
        XYSeries outliers = new XYSeries("OUTLIERS"); 
        for(int j=0;j<out.size();j++)
        {
            TSPVertex v=out.get(j);
            outliers.add(v.getX(),v.getY());
        }
        dataset.addSeries(outliers);
       
        return dataset;
    }
    
    //come il metodo precedente, ma nel caso in cui non si tiene conto degli outlier
    private static XYDataset createDataset(List<List<TSPVertex>> l, List<TSPVertex> center) {
        return createDataset(l, center,null);
    }
    
    //metodo statico per la creazione dei grafici se non si tiene conto degli outlier
    public static void grafico( List<List<TSPVertex>> l,List<TSPVertex> center,String tit) {
        Grafici chart = new Grafici(l,center,createDataset(l,center),tit);
        chart.pack( );          
        RefineryUtilities.centerFrameOnScreen( chart );          
        chart.setVisible( true ); 
    }
   
    //metodo statico per la creazione dei grafici se si tiene conto degli outlier
    public static void grafico( List<List<TSPVertex>> l,List<TSPVertex> center,List<TSPVertex> outlier,String tit) {
        Grafici chart = new Grafici(l,center,createDataset(l,center,outlier),tit);
        chart.pack( );          
        RefineryUtilities.centerFrameOnScreen( chart );          
        chart.setVisible( true ); 
    }
}