package datamining;

import org.apache.spark.util.AccumulatorV2;

//classe che estende AccumulatorV2
//gli accumulatori vengono utilizzati in spark per aggiornare in maniera sicura
//delle variabili che sono condivise da più macchine
//nel caso specifico, l'accumulatore viene usato per aggiornare il valore della funzione obiettivo
public class ObjectiveValueAccumulator extends AccumulatorV2{
    private double opt_val;
    
    public ObjectiveValueAccumulator(double d)
    {
        opt_val=d;
    }
    
    public ObjectiveValueAccumulator()
    {
        opt_val=0;
    }
    
    @Override
    public boolean isZero() {
        return opt_val==0;
    }

    @Override
    public AccumulatorV2 copy() {
        return new ObjectiveValueAccumulator(opt_val);
    }

    @Override
    public void reset() {
        opt_val=0;
    }
    
    //viene fatto un ABUSO DEL METODO ADD, che dovrebbe servire solo per incrementare
    //la variabile in questione.
    //in questo caso al metodo add viene passato un nuovo valore double
    //se tale valore è maggiore del valore attuale della funzione obiettivo,
    //questo viene aggiornato.
    @Override
    public void add(Object in) {
        if(!(in instanceof Double))
            return;
        double di=(Double)in;
        if(di>opt_val)
            opt_val=di;
    }

    @Override
    public void merge(AccumulatorV2 av) {
        this.add((Double)av.value());
    }

    @Override
    public Object value() {
        return new Double(opt_val);
    }
}
