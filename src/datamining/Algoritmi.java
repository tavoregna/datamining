package datamining;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

//classe che contiene l'implementazione degli algoritmi
//presenti nel paper
//e un metodo per il calcolo del silhouette coefficiente
public  class Algoritmi {
    //Algoritmo k-center (senza outlier) sequenziale
    //viene anche utilizzato come subroutine da GREEDY-MR
    //ne è stata ottimizzata l'implementazione per far si che ciascun punto 
    //tenga traccia del centro piu vicino cosi da evitare di calcolare ogni volta la distanza
    //di quel punto da X
   
    /*
    NOTA:
    GREEDY1 è ottimizzato per OUTLIER-MR
    GREEDY è ottimizzato come algoritmo sequenziale
    GREEDY_for_greedy_mr è ottimizzato per GREEDY-MR
    
    questi 3 algoritmi fanno esattamente le stesse cose, ottimizzate per il caso specifico
    */
    public static<T>  GREEDY_OUTPUT<T> GREEDY1(List<PointCenter<T>> U,int k,Distance d)
    {     
        if(U.size()<k)                                               //controllo che il numero di elementi elaborati
        {                                                            //sia maggiore del numero di centri
            System.out.println("parametro k non valido");
            System.exit(1);
        }
        
        List<T> X=new ArrayList<>();                                    // X=vuoto
        X.add(U.get((int)(Math.random()*U.size())).getPoint());         //aggiungi un qualunque appartenente a U a X (primo punto preso in modo casuale)    
        T last_center_added=X.get(0);
        
        while(X.size()<k)                                    //mentre |X|<k
        {
            double max=0;                                    //cerco il punto v appartenente a U piu distante dall'insieme X corrente
            T v=null;                                        //NOTA: la distanza di v da X è la distanza di v dal suo centro più vicino di cui sto tenendo traccia
            Iterator<PointCenter<T>> i=U.iterator();
            while(i.hasNext())
            {
                PointCenter<T> p=i.next();
                p.tryUpdateCenter(last_center_added,X.size()-1);  //verifico se l'ultimo centro trovato è piu vicino del centro attuale, in tal caso aggiorno il centro più vicino a quel punto
                double dist=p.getDist();
                if(dist>max)
                {
                    v=p.getPoint();
                    max=dist;
                }
            }
            X.add(v);                                        //X = X unito {v}
            last_center_added=v;
        }
        
        Iterator<PointCenter<T>> i=U.iterator();                    //provo a vedere se l'ultimo centro trovato
        while(i.hasNext())                                          //è vicino a qualche punto in U  
            i.next().tryUpdateCenter(last_center_added,k-1);      
        
        return new GREEDY_OUTPUT<>(X,U);
    }
    
    public static<T>  GREEDY_OUTPUT<T> GREEDY(List<T> U,int k,Distance d)
    {         
        List<PointCenter<T>> UU=new ArrayList<>();
        U.forEach(cnsmr->UU.add(new PointCenter(cnsmr, d)));
        return GREEDY1(UU,k,d);
    }
    
    public static<T>  List<T> GREEDY_for_greedy_mr(List<T> U,int k,Distance d)    //algoritmo GREEDY ottimizzato per GREEDY-MR
    {         
        if(U.size()<k)                                               //controllo che il numero di elementi elaborati
        {                                                            //sia maggiore del numero di centri
            System.out.println("parametro k non valido");
            System.exit(1);
        }
        
        List<T> X=new ArrayList<>();                            // X=vuoto
        X.add(U.get((int)(Math.random()*U.size())));         //aggiungi un qualunque appartenente a U a X (primo punto preso in modo casuale)
        List<PointCenter<T>> UU=new ArrayList<>();
        U.forEach(cnsmr->UU.add(new PointCenter(cnsmr, d))); //sistemare
       
        T last_center_added=X.get(0);
        
        while(X.size()<k)                                    //mentre |X|<k
        {
            double max=0;                                    //cerco il punto v appartenente a U piu distante dall'insieme X corrente
            T v=null;                                        //NOTA: la distanza di v da X è la distanza di v dal suo centro più vicino di cui sto tenendo traccia
            Iterator<PointCenter<T>> i=UU.iterator();
            while(i.hasNext())
            {
                PointCenter<T> p=i.next();
                p.tryUpdateCenter(last_center_added,X.size()-1);  //verifico se l'ultimo centro trovato è piu vicino del centro attuale, in tal caso aggiorno il centro più vicino a quel punto
                double dist=p.getDist();
                if(dist>max)
                {
                    v=p.getPoint();
                    max=dist;
                }
            }
            X.add(v);                                        //X = X unito {v}
            last_center_added=v;
        }
        
        return X;
    }
    
    //Algoritmo k-center con outlier sequenziale
    public static<T>   OUTLIER_OUTPUT<T> OUTLIERS(List<T> U,int k,double G,Distance d)
    {
        List<T> X=new ArrayList<>();                        //X = vuoto
        List<T> U1=new ArrayList<>(U);                       //U1 = U
                             //bv conterrà tutti i punti che distano da v (scelto) al piu 3*G
        while(X.size()<k)                                   //mentre |X| < k
        {
            int max=0;                                      //cerco il punto v' in U1 che ha piu punti vicini
            T v=null;                                       //ovvero che ha più punti in U' che distano al più G da esso
            List<T> bv=new ArrayList<>();      
            
            Iterator<T> i=U1.iterator();
            while(i.hasNext())
            {
                T u=i.next();
                int count=0;
                Iterator<T> v1=U1.iterator();
                List<T> temp3G=new ArrayList<>();
                while(v1.hasNext())
                {
                    T el=v1.next();
                    double dis=d.distanceBetweenPoints(u,el);
                    if(dis<=G)
                        count++;
                    if(dis<=3*G)                                //in contemporanea tengo traccia di tutti i punti in U' che distano al più 3*G dal punto che andrò a scegliere
                        temp3G.add(el);                         //così facendo determino Bv'
                }
                if(count>max)
                {
                    max=count;
                    v=u;
                    bv=temp3G;
                }
            }
            X.add(v);                                       //X = X unito {v}
                               
            U1.removeAll(bv);                               //U'=U'-Bv'

            if(U1.isEmpty() && X.size()<k)   //ho svuotato U' troppo velocemente 
            {
                System.err.println("Valore di G troppo elevato");
                System.exit(1);
            }
        }
        return new OUTLIER_OUTPUT<>(X,U1);                  //i punti rimasti in U' sono outlier
    }
    
    //Algoritmo PARTITION sequenziale, usato dopo aver eseguito OUTLIER
    //vuole come parametri il dataset, la lista dei centri e la lista degli outlier.
    public static<T>  PARTITION_OUTPUT<T> PARTITION(List<T> U,List<T> center,List<T> out,Distance d)
    {
        if(U==null || center==null || U.isEmpty() || center.isEmpty())
            return null;
        
        if(out!=null)                               //se ho degli outlier, non li devo considerare ("li rimuovo dal dataset")
        {
            U=new ArrayList<>(U);                   //List ottenuto da JavaRdd com collect() non supporta removeAll() quindi si è dovuto fare questo "travaso"
            U.removeAll(out);
        }
        double obj_val=0;                           //Inizializzo valore della funzione obiettivo
        
        List<List<T>> clus=new ArrayList<>();       //predispongo l'array che conterrà il clustering
        for(int i=0;i<center.size();i++)
            clus.add(new ArrayList<>());
        
        Iterator<T> it=U.iterator();
        while(it.hasNext())
        {
            T t=it.next();
            int nearest_center_index=0;             //cerco il centro più vicino a ciascun punto
            double dist_nearest_center=d.distanceBetweenPoints(t, center.get(0));
            for(int i=1;i<center.size();i++)
                {
                    double dis=d.distanceBetweenPoints(t, center.get(i));
                    if(dis<dist_nearest_center)
                    {
                        nearest_center_index=i;
                        dist_nearest_center=dis;
                    }
                }
                if(dist_nearest_center>obj_val)                    //ho trovato un punto "lontano" dal suo centro più vicino quindi
                    obj_val=dist_nearest_center;                   //aggiorno la funzione obiettivo
                clus.get(nearest_center_index).add(t);             //metto il punto nel giusto cluster
        }
        return new PARTITION_OUTPUT<>(obj_val,clus);
    }
    
    //come il metodo sopra ma senza outlier
    public static<T>  PARTITION_OUTPUT<T> PARTITION(List<T> U,List<T> center,Distance d)
    {
        return PARTITION(U,center,null,d);
    }
    
    //Algoritmo PARTITION specifico per GREEDY (sequenziale) che sfrutta l'informazione aquisita 
    //ovvero sapendo già qual è il centro più vicino a ciascun punto, posso evitare di ricalcolarlo per ciascun punto
    //quindi in questa variante di partition la complessità passa da O(N*K) a O(N)
    public static<T>  PARTITION_OUTPUT<T> PARTITION(List<PointCenter<T>> u,int k)
    {
        if(u==null || u.isEmpty())
            return null;
        
        double obj_val=0;                           //Inizializzo valore della funzione obiettivo
        
        List<List<T>> clus=new ArrayList<>();       //predispongo l'array che conterrà il clustering
        for(int i=0;i<k;i++)
            clus.add(new ArrayList<>());
        
        Iterator<PointCenter<T>> it=u.iterator();
        while(it.hasNext())
        {
            PointCenter<T> t=it.next();
                clus.get(t.getCenter_num()).add(t.getPoint());  //metto ciascun punto nel giusto cluster
            if(t.getDist()>obj_val)                             //ho trovato un punto "lontano" dal suo centro più vicino quindi
                obj_val=t.getDist();                            //aggiorno la funzione obiettivo
        }
        return new PARTITION_OUTPUT<>(obj_val,clus);
    }
    
    //Algoritmo PARTITION distribuito, da usare dopo l'esecuzione di GREEDY-MR e OUTLIER-MR
    public static<T>  PARTITION_OUTPUT<T> PARTITION_MR(JavaSparkContext sc,JavaRDD<T> u,List<T> center,List<T> out,Distance d)
    {
        if(u==null || center==null || u.isEmpty() || center.isEmpty())
            return null;
       
        if(out!=null)                                                       //se ci sono degli outlier, li rimuovo dal dataset
        {   
            List<T> temp=new ArrayList<>(u.collect());                      //si è dovuto procedere in questo modo in quanto la funzione subtract() di spark
            temp.removeAll(out);                                            //dava qualche problema
            u=sc.parallelize(temp,sc.defaultParallelism());
        }
        
        Broadcast<List<T>> centriBroad = sc.broadcast(center);   //variabile di broadcast per distribuire a ciascuna macchina l'insieme dei k centri
        ObjectiveValueAccumulator obj_val=new ObjectiveValueAccumulator();  //varabile accumulatrice per l'aggiornamento del valore della funzione obiettivo
        sc.sc().register(obj_val,"e");
        
        List<List<T>> clus=u.mapToPair((t) -> {                                     //l'idea è quella di dare a ciascuna macchina una partizione degli elementi del dataset 
            int nearest_center_index=0;                                             //e poi trovare il centro più vicino a ciascun punto della partizione
            List<T> cente=centriBroad.getValue();
            for(int i=1;i<cente.size();i++)
            {
                if(d.distanceBetweenPoints(t, cente.get(i))<d.distanceBetweenPoints(t, cente.get(nearest_center_index)))
                    nearest_center_index=i;
            }
            double dist=d.distanceBetweenPoints(t, cente.get(nearest_center_index));
            obj_val.add(dist);
            Tuple2<Integer,T> tup=new Tuple2(nearest_center_index,t);
                return tup;
            })
            .groupByKey()
            .map((t)->{
                List<T> lif=new ArrayList<>();
                t._2().forEach((cnsmr)->lif.add(cnsmr));
                return lif;
            })
            .collect();
        
        return new PARTITION_OUTPUT<>((double)obj_val.value(),clus);
    }
    
    //come il metodo sopra a senza outlier
    public static<T>  PARTITION_OUTPUT<T> PARTITION_MR(JavaSparkContext sc,JavaRDD<T> u,List<T> center,Distance d)
    {
        return PARTITION_MR(sc, u, center, null, d);
    }
    
    //implementazione algoritmo per k-center clustering (senza outlier) distribuito
    public static<T> List<T> GREEDY_MR(JavaSparkContext sc,JavaRDD<T> U,int  k,Distance d)
    {
        if(U.count()/sc.defaultParallelism()<k)                      //controllo che il numero di elementi in ciascuna partizione di U
        {                                                            //sia maggiore del numero di centri
           System.out.println("parametro k non valido");
           System.exit(1);
        }
        
        return U
        .mapPartitions((t) -> {
            List<T> Ui=new ArrayList<>();                                                       
            t.forEachRemaining((el)->Ui.add(el));
            List<T> Ci=GREEDY_for_greedy_mr(Ui,k,d);                //Ogni macchina i calcola Ci=GREEDY(Ui,k)
            return Ci.iterator(); 
        })
        .coalesce(1,true) //porta il numero di partizioni a 1, equivale a passare l'unione dei Ci alla macchina 1.
        .mapPartitions((t) -> {
            List<T> union_Ci=new ArrayList<>();
            t.forEachRemaining((el)->union_Ci.add(el));
            List<T> X=GREEDY_for_greedy_mr(union_Ci,k,d);       //la macchina 1 calcola X=GREEDY(Unione Ci,k)
            return X.iterator(); 
        })
        .cache()
        .collect();
    }
    
    //implementazione algoritmo per k-center clustering con outlier distribuito
    public static<T> OUTLIER_OUTPUT<T> OUTLIERS_MR(JavaSparkContext sc,JavaRDD<T> U,int  k,int z,double G,Distance d)
    {
        if(U.count()/sc.defaultParallelism()<k+z)                   //controllo che il numero di elementi in ciascuna partizione di U
        {                                                           //sia maggiore del numero di centri più il numero di outlier
           System.out.println("parametri k e z non validi");
           System.exit(1);
        }
        return U
            .mapPartitionsToPair((t) -> {                     //questo primo round è il collo di bottiglia al tempo complessivo di esecuzione
                List<PointCenter<T>> Ui=new ArrayList<>();           
                t.forEachRemaining((el)->Ui.add(new PointCenter<>(el,d)));
                GREEDY_OUTPUT<T> output_greedy=GREEDY1(Ui,k+z,d);             //Ogni macchina i calcola Ci=GREEDY(Ui,k+z)
                List<PointCenter<T>> supp=output_greedy.getPointCenter();
                List<T> Ci=output_greedy.getCenter();
                List<Tuple2<T,Integer>> c_wc=new ArrayList<>();
                Ci.forEach((r)->c_wc.add(new Tuple2<>(r,1)));
                supp.forEach((el)->{                                //Ogni macchina calcola, per ogni punto c in Ci,
                    double distToSet=el.getDist();                  //Wc, ovvero il numero di punti in Ui che hanno c come centro
                    T point=el.getPoint();                          //più vicino rispetto agli altri in Ci
                    for(int i=0;i<c_wc.size();i++)
                    {
                        T cen=c_wc.get(i)._1();     
                        if(d.distanceBetweenPoints(point, cen)==distToSet)
                        {
                            c_wc.set(i,new Tuple2(cen,c_wc.get(i)._2().intValue()+1));
                        }
                    }
                });
                return c_wc.iterator(); 
             })
            .coalesce(1,true)                              //porta il numero di partizioni a 1, equivale a passare l'unione dei Ci e i relativi pesi alla macchina 1.
            .mapPartitions((t) -> {         
                List<Tuple2<T,Integer>> r=new ArrayList<>();
                t.forEachRemaining((p)->r.add(p));
                List<OUTLIER_OUTPUT<T>> X=new ArrayList<>();    //X = CLUSTER(unione Ci(e relativi pesi),k,G)
                X.add(CLUSTER(r,k,G,d));  
                return X.iterator();
            })
            .cache()
            .first();
    }
    
    //subroutine di OUTLIER-MR
    public static<T>  OUTLIER_OUTPUT<T> CLUSTER(List<Tuple2<T,Integer>> u,int k,double G,Distance d)
    {
        List<T> x=new ArrayList<>();                              // X = vuoto
        List<Tuple2<T,Integer>> u1=new ArrayList<>();             // U' = U
        u.forEach((l)->u1.add(l));
        while(x.size()<k)                                           //mentre |X|<k
        {
            int max=0;
            Iterator<Tuple2<T,Integer>> i=u.iterator();
            List<Tuple2<T,Integer>> bv=new ArrayList<>();
            Tuple2<T,Integer> v=null;
            while(i.hasNext())                                          //cenrco il punto v che ha la maggiore somma dei pesi dei punti in U' che 
            {                                                           //stanno a distanza al più 5*G da v
                Tuple2<T,Integer> p1=i.next();
                List<Tuple2<T,Integer>> temp=new ArrayList<>();
                int sum=0;
                Iterator<Tuple2<T,Integer>> i1=u1.iterator();
                while(i1.hasNext())
                {
                    Tuple2<T,Integer> nex=i1.next();
                    double dis=d.distanceBetweenPoints(p1._1(),nex._1());
                    if(dis<=5*G)
                        sum+=nex._2();
                    if(dis<=11*G)                                       //contemporaneamente tengo tracia di tutti i punti in U' che stanno a distanza al più 11*G 
                        temp.add(nex);                                  //dal v che andrò a scegliere (insieme Bv'
                }
                if(sum>max)
                {
                    max=sum;
                    v=p1;
                    bv=temp;
                }
            }
            x.add(v._1());                                          //X = X unito {v}

            u1.removeAll(bv);                                       //U' = U'-Bv'
            if(u1.isEmpty() && x.size()<k)   //ho svuotato U' troppo velocemente 
            {
                System.err.println("Valore di G troppo elevato");
                System.exit(1);
            }
        }
        List<T> out=new ArrayList<>();
        u1.forEach((cnsmr)->out.add(cnsmr._1()));                           //trasformazione da coppie (v,wv) a (v)
        return new OUTLIER_OUTPUT<>(x,out);                                 //i punti rimasti in U' sono outlier
    }   
    
    //Metodo per il calcolo del Silhouette coefficient
    //NOTA: il seguente algoritmo restituisce il coefficiente di Silhouette medio su tutti i punti del clustering
    //è molto oneroso computazionalmente O(n^2)
    public static<T> double Silhouette_coefficient(List<List<T>> cl,Distance d)
    {
        double sum=0;
        int n=0;
        
        for(int i=0;i<cl.size();i++)
        {
            n+=cl.get(i).size();
        }
        
        for(int i=0;i<cl.size();i++)
        {
            for(int j=0;j<cl.get(i).size();j++)
            {
                T point=cl.get(i).get(j);
                double ap=averageDistanceBetweenPandC(point,cl.get(i),d);                   //calcolo ap
                double bp=averageDistanceBetweenPandC(point,cl.get((i==0)?1:0),d);          //calcolo bp
                for(int k=1;k<cl.size();k++)
                {
                    if(k==i)
                        continue;
                    double temp=averageDistanceBetweenPandC(point,cl.get(k),d);
                    if(temp<bp)
                        bp=temp;
                }
                sum+=(bp-ap)/(Math.max(ap, bp)*n);                              
            }
        }
        return sum;                                                 
    }
    
    //subroutine utilizzata per il calcolo del coefficiente di silhouette
    //calcola la distanza media tra un punto p e tutti i punti in C
    private static <T> double averageDistanceBetweenPandC(T p,List<T> C,Distance d)
    {
        int count=0;
        double sum=0;
        for(int i=0;i<C.size();i++)
        {
            T el=C.get(i);
            if(el.equals(p))
                continue;
            sum+=d.distanceBetweenPoints(p, el);
            count++;
        }
        return sum/count;
    }
    
    //scelta di k centri in modo casuale
    public static <T> List<T> randomCenter(JavaRDD<T> u,int k)
    {
        List<T> U=u.collect();
        List<T> cen=new ArrayList();
        while(cen.size()<k)
        {
            int ran=(int)(Math.random()*U.size());
            T el=U.get(ran);
            if(!cen.contains(el))
                cen.add(el);
        }
        return cen;
    }
}