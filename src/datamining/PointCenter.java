package datamining;

//classe che rappresenta una coppia punto-centro e la distanza tra di essi.
//viene usata per rendere più efficienti gli algoritmi GREEDY, GREEDY1 e GREEDY_for_greedy_mr
//(e di conseguenza tutti gli algoritmi che li utilizzano)
//tenendo traccia ad ogni iterazione del centro più vicino a ciascun punto.
//in questo modo posso efficientemente calcolare la distanza di un punto dall'insieme dei centri
// e la complessità degli algoritmi sopracitati passa da
//O(n*k^2) a O(N*k)
public class PointCenter <T>{
    private final T point;
    private T center;
    private int center_num;
    private double dist;
    private final Distance d;

    public PointCenter(T point,Distance d) {
        this.point = point;
        this.center = null;
        this.dist = 0;
        this.d = d;
        this.center_num = 0;
    }
    
    public T getPoint() {
        return point;
    }
    
    public double getDist() {
        return dist;
    }

    public int getCenter_num() {
        return center_num;
    }
    
    //verifico se il centro cent passato come parametro è più vicino del centro attuale
    //in tal caso aggiorno il centro più vicino e la distanza
    public boolean tryUpdateCenter(T cent,int cnum) {
        if(center==null)
        {
            center=cent;
            dist=d.distanceBetweenPoints(point, center);
            center_num=cnum;
            return true;
        }
        double new_dis=d.distanceBetweenPoints(point, cent);
        if(new_dis<dist)
        {
            center=cent;
            dist=new_dis;
            center_num=cnum;
            return true;
        }
        return false;
    }
}
