package datamining;

//interfaccia che deve essere implementata dalle classi che rappresentano
//punti di dataset
//in particolare le classi devono implementare il metodo toVector()
//che viene utilizzato per il calcolo delle distanze
public interface Confrontabile {
    public Double[] toVector();
}
