package datamining;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

//Classe contenente metodi di utilità generale
public class Utility {
    //Metodo d'utilità per salvare sul file con percorso path il clustering fatto su un'immagine
    public static void partitionToImage(List<List<RGBpixel>> clus,List<RGBpixel> cen,String path) 
    {
        try {
            File file = new File(path);
            BufferedImage old_image;
            old_image = ImageIO.read(file);
            
            BufferedImage new_image=new BufferedImage(old_image.getWidth(), old_image.getHeight(), 1);
            
            for(int i=0;i<cen.size();i++)
            {
                RGBpixel c=cen.get(i);
                for(int j=0;j<clus.get(i).size();j++)
                {
                    int x=clus.get(i).get(j).getX();
                    int y=clus.get(i).get(j).getY();
                    new_image.setRGB(x,y,c.getRGB());
                }               
            }
            File output = new File(".\\data\\image-cluster-k-"+cen.size()+".jpg");
            ImageIO.write(new_image, "jpg", output);            
        }
        catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Metodo che permette di scrivere su un file (in modalità append)
    public static boolean addLineToFile(String path,String line)
    {
        try {
            BufferedWriter output;
            output = new BufferedWriter(new FileWriter(path,true));  //clears file every time
            output.append(line);
            output.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    //Metodo per la stampa del clustering ottenuto dopo l'esecuzione di PARTITION o PARTITION_MR
    public static <T> void  stampaCluster(List<List<T>> clus)
    {
        for(int i=0;i<clus.size();i++)
        {
            System.out.println("CLUSTER: "+(i+1));
            List<T> cl=clus.get(i);
            cl.forEach(el->System.out.println(el));
            System.out.println();
        }
    }
}
