Il programma � stato sviluppato utilizzando l'IDE Netbeans.
Per sfruttare al meglio le funzionalit� del programma consiglio di aprirlo proprio con Netbeans e di eseguirlo da li

Tutte le librerie necessarie al corretto funzionamento sono situate nella directory libs

I sorgenti sono situati nella directory src\datamining
Il file contenente il metodo main � DataMining.java
I file relativi a ciascuna classe contengono dei commenti che ne specificano lo scopo

Nella directory data sono presenti i dataset
Nella directory result sono presenti i risultati degli esperimenti svolti
Nella cartella null\bin � presente il file winutils.exe, specifico per il corretto funzionamento di spark in windows

Sono stati fatti anche degli esperimenti di clustering su immagini, situati nella cartella result\Image clustering,
che non sono stati riportati nella relazione
Sono interessanti, dateci un'occhiata

Le istruzioni, riportate nella classe DataMining.java, sono:
1. "scommentare"/"commentare" i blocchi relativi agli algoritmi che si vogliono/non si vogliono eseguire
2. una volta scelto il dataset, bisogna mettere nella variabile datapath il giusto percorso del dataset
3. impostare tra parentesi angolari  (<  >) il giusto tipo di dato, relativo al dataset scelto
4. settare opportunamente (da codice o da riga di comando) i parametri k,z,m e soprattutto G
NOTA: � possibile rappresentare graficamente i risultati del clustering solo per i dataset .tsp

La classe DataMining.java permette l'esecuzione degli algoritmi (con relativi risultati):
-GREEDY (sequenziale)
-OUTLIERS (sequenziale)
-GREEDY_MR (distribuito)
-OUTLIERS_MR (distribuito)
� stata anche implementata la possibilit� di scelta dei k centri in modo casuale
e la possibilit� di visualizzare il clustering ottenuto (dataset .tsp)

di default, vengono eseguiti gli algoritmi GREEDY_MR e OUTLIERS_MR sul dataset p654.tsp
(� stato scelto questo dataset poich�, essendo piccolo, � possibile calcolare il coefficiente di Silhouette e rappresentare il clustering ottenuto in modo veloce)

si devono settare i parametri 
-Dspark.default.parallelism       (che rappresenta il numero di macchine m)
-Dspark.master (se possibile settarlo a 8)
da riga di comando si possono passare i parametri (nell'ordine)
k z G
(la scelta di G � critica)
per p654.tsp consiglio di mettere i parametri: m=24, k=8, z=10, G=75

LEGENDA colori grafici:
* Blu: 		GREEDY
* Rosso: 	GREEDY-MR
* Verde: 	OUTLIER
* Nero: 	OUTLIER-MR